﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Ordering.Models;

namespace Ordering
{
    public partial class FormMain : Form
    {
        private const decimal TAX_RATE = .0825m;
        private readonly List<Category> categories;
        private Check check;

        public FormMain()
        {
            InitializeComponent();

            this.categories = MenuCreator.CreateMenu();
            this.check = new Check(TAX_RATE);
        }

        /// <summary>
        /// Use databinding to bind all the data to controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormMain_Load(object sender, EventArgs e)
        {
            // Add data source and specify the property name as the display string
            this.cboCategories.DataSource = categories;
            this.cboCategories.DisplayMember = "Name";

            // Use Description field as DisplayMember to format text to how we want to look
            this.lstMenuItems.DataSource = categories[0].MenuItems;
            this.lstMenuItems.DisplayMember = "Description";

            this.lstCheckItems.DataSource = this.check.MenuItems;
            this.lstCheckItems.DisplayMember = "Description";

            // Although we don't take full advantage of DataBinding the text fields, do it anyways to follow the pattern.
            // If we set the property, these will automatically update
            this.txtSubTotal.DataBindings.Add("Text", this.check, "SubTotal", true, DataSourceUpdateMode.OnPropertyChanged, 0, "C");
            this.txtTaxTotal.DataBindings.Add("Text", this.check, "TaxTotal", true, DataSourceUpdateMode.OnPropertyChanged, 0, "C");
            this.txtTotal.DataBindings.Add("Text", this.check, "Total", true, DataSourceUpdateMode.OnPropertyChanged, 0, "C");
        }

        /// <summary>
        /// Add to MenuItem to check
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lstMenuItems_DoubleClick(object sender, EventArgs e)
        {
            // The DataSource is a list of OrderItems so the SelectedItem would be an OrderItem
            var menuItem = (OrderItem)this.lstMenuItems.SelectedItem;

            this.check.MenuItems.Add(menuItem);

            // Rebind the items
            this.lstCheckItems.DataSource = null;
            this.lstCheckItems.DataSource = this.check.MenuItems;
            this.lstCheckItems.DisplayMember = "Description";

            // Update Totals (if we set the property, these fields would've been updated automatically)
            this.txtSubTotal.DataBindings.Clear();
            this.txtTaxTotal.DataBindings.Clear();
            this.txtTotal.DataBindings.Clear();
            this.txtSubTotal.DataBindings.Add("Text", this.check, "SubTotal", true, DataSourceUpdateMode.OnPropertyChanged, 0, "C");
            this.txtTaxTotal.DataBindings.Add("Text", this.check, "TaxTotal", true, DataSourceUpdateMode.OnPropertyChanged, 0, "C");
            this.txtTotal.DataBindings.Add("Text", this.check, "Total", true, DataSourceUpdateMode.OnPropertyChanged, 0, "C");
        }

        /// <summary>
        /// Refresh menu items from category
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cboCategories_SelectedIndexChanged(object sender, EventArgs e)
        {
            // The DataSource is a list of Categories so the SelectedItem would be a Category
            var category = (Category)this.cboCategories.SelectedItem;
            
            // Rebind the items
            this.lstMenuItems.DataSource = null;
            this.lstMenuItems.DataSource = category.MenuItems;
            this.lstMenuItems.DisplayMember = "Description";
        }

        private void btnPay_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Check paid. Thank you for ordering!", "Paid", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.check = new Check(TAX_RATE);

            // Reset
            this.lstCheckItems.DataSource = null;

            this.txtSubTotal.DataBindings.Clear();
            this.txtTaxTotal.DataBindings.Clear();
            this.txtTotal.DataBindings.Clear();

            this.txtSubTotal.DataBindings.Add("Text", this.check, "SubTotal", true, DataSourceUpdateMode.OnPropertyChanged, 0, "C");
            this.txtTaxTotal.DataBindings.Add("Text", this.check, "TaxTotal", true, DataSourceUpdateMode.OnPropertyChanged, 0, "C");
            this.txtTotal.DataBindings.Add("Text", this.check, "Total", true, DataSourceUpdateMode.OnPropertyChanged, 0, "C");
        }
    }
}
