﻿using System;
using System.Collections.Generic;
using Ordering.Models;

namespace Ordering
{
    public static class MenuCreator
    {
        public static List<Category> CreateMenu()
        {
            return
                new List<Category>
                {
                    new Category(Guid.NewGuid(), "Burgers")
                    {
                        MenuItems = new List<OrderItem>
                        {
                            new OrderItem(Guid.NewGuid(), "Hamburger", 2.99m),
                            new OrderItem(Guid.NewGuid(), "Chz Hamburger", 3.99m),
                            new OrderItem(Guid.NewGuid(), "Double Chz Hamburger", 4.99m),
                            new OrderItem(Guid.NewGuid(), "Big Mac", 5.99m),
                            new OrderItem(Guid.NewGuid(), "Grilled Chkn Burger", 4.99m),
                            new OrderItem(Guid.NewGuid(), "Veggie Burger", 3.99m),
                        }
                    },

                    new Category(Guid.NewGuid(), "Drinks")
                    {
                        MenuItems = new List<OrderItem>
                        {
                            new OrderItem(Guid.NewGuid(), "Coke", 1.99m),
                            new OrderItem(Guid.NewGuid(), "Diet Coke", 1.99m),
                            new OrderItem(Guid.NewGuid(), "Dr. Pepper", 1.49m),
                            new OrderItem(Guid.NewGuid(), "Mellow Yellow", 2.99m),
                            new OrderItem(Guid.NewGuid(), "Ginger Ale", 2.50m),
                        }
                    }
                };
        }
    }
}
