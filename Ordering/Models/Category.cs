﻿using System;
using System.Collections.Generic;

namespace Ordering.Models
{
    public class Category
    {
        public Category(Guid id, string name)
        {
            this.Id = id;
            this.Name = name;
            this.MenuItems = new List<OrderItem>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }

        public List<OrderItem> MenuItems { get; set; }
    }
}
