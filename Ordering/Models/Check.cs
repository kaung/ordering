﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Ordering.Models
{
    public class Check
    {
        public Check(decimal taxRate)
        {
            this.TaxRate = taxRate;
            this.MenuItems = new List<OrderItem>();
        }

        public decimal TaxRate { get; set; }

        public decimal TaxTotal
        {
            get { return Math.Round(this.SubTotal * this.TaxRate, 2); }
        }

        public decimal SubTotal
        {
            get { return this.MenuItems.Sum(m => m.Price); }
        }

        public decimal Total
        {
            get { return this.SubTotal + this.TaxTotal; }
        }

        public List<OrderItem> MenuItems { get; set; }
    }
}
