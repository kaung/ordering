﻿using System;

namespace Ordering.Models
{
    public class OrderItem
    {
        public OrderItem(Guid id, string name, decimal price)
        {
            this.Id = id;
            this.Name = name;
            this.Price = price;
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }

        public string Description
        {
            get { return String.Format("{0}{1:C}", this.Name.PadRight(25), this.Price); }
        }
    }
}
