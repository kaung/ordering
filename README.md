Introduction
============

Simple ordering app utilizing data binding with WinForm controls.

## Dependencies

- [git](https://git-scm.com)
- [Visual Studio 2013 or higher](https://www.visualstudio.com/en-us/products/visual-studio-express-vs.aspx)


## Install

```sh
> git clone https://bitbucket.org/kaung/ordering
```

## Run

Open Ordering.sln in VS2013 then press F5
